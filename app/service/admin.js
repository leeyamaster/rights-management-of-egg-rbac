'use strict';

const Service = require('egg').Service;

var url = require('url');

class AdminService extends Service {
	async checkAuth() {
		/*
		    1、获取当前用户的角色        （忽略权限判断的地址    is_super）
		    2、根据角色获取当前角色的权限列表
		    3、获取当前访问的url 对应的权限id
		    4、判断当前访问的url对应的权限id 是否在权限列表中的id中
		*/
		//1、获取当前用户的角色
		const userinfo = this.ctx.session.userinfo;
		const role_id = userinfo.role_id;
		const pathname = url.parse(this.ctx.request.url).pathname; //获取当前用户访问的地址
		const method = this.ctx.request.method;
		//忽略权限判断的地址    is_super表示是管理员
		const ignoreUrl = ['/admin/login', '/admin/verify', '/admin/loginOut', '/admin/getRoleId'];
		if (ignoreUrl.indexOf(pathname) != -1 || userinfo.is_super == 1) {
			return true; //允许访问
		}
		//2、根据角色获取当前角色的权限列表
		let accessResult = await this.ctx.model.RoleAccess.find({
			"role_id": role_id
		});
		accessResult = accessResult.map(v => {
			return v.access_id.toString()
		})
		// 3、获取当前访问的url 对应的权限id
		let accessUrlResult = await this.ctx.model.Access.find({
			"url": pathname
		});
		//4、判断当前访问的url对应的权限id 是否在权限列表中的id中
		if (accessUrlResult.length > 0) {
			let f = false;
			accessUrlResult.forEach(v => {
				if ((accessResult.indexOf(v._id.toString()) != -1) && v.method == method) {
					f = true;
				}
			})
			if (f) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	//获取权限列表的方法
	async getAuthList(role_id) {
		let result = await this.ctx.model.Access.aggregate([{
				$lookup: {
					from: 'access',
					localField: '_id',
					foreignField: 'module_id',
					as: 'items'
				}
			},
			{
				$match: {
					"module_id": '0'
				}
			},
			{
				$sort: {
					sort: -1
				}
			}
		]);

		//2、查询当前角色拥有的权限（查询当前角色的权限id） 把查找到的数据放在数组中
		var accessReulst = await this.ctx.model.RoleAccess.find({
			"role_id": role_id
		});
		accessReulst = accessReulst.map((v) => {
			return v.access_id.toString();
		})

		// 3、循环遍历所有的权限数据     判断当前权限是否在角色权限的数组中
		result.forEach(v => {
			if (accessReulst.indexOf(v._id.toString()) != -1) {
				v.checked = true;
			}
			v.items.forEach(item => {
				if (accessReulst.indexOf(item._id.toString()) != -1) {
					item.checked = true;
				}
			})
		})
		// for (var i = 0; i < result.length; i++) {
		//     if (accessReulst.indexOf(result[i]._id.toString()) != -1) {
		//         result[i].checked = true;
		//     }
		//     for (var j = 0; j < result[i].items.length; j++) {
		//         if (accessReulst.indexOf(result[i].items[j]._id.toString()) != -1) {
		//             result[i].items[j].checked = true;
		//         }
		//     }
		// }
		return result;
	}
}

module.exports = AdminService;
