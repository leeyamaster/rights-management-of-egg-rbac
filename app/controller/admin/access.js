'use strict';

let BaseController = require('./base.js');

class AccessController extends BaseController {
	async index() {
		// let result = await this.ctx.model.Access.find({});
		// 1、在access表中找出  module_id=0的数据        管理员管理 _id    权限管理 _id    角色管理  (模块)
		// 2、让access表和access表关联    条件：找出access表中  module_id等于_id的数据
		let result = await this.ctx.model.Access.aggregate([{
				$lookup: {
					from: 'access',
					localField: '_id',
					foreignField: 'module_id',
					as: 'items'
				}
			},
			{
				$match: {
					"module_id": '0'
				}
			},
			{
				$sort: {
					sort: -1
				}
			},
		])
		this.success(200, "请求成功", result)
	}

	//获取模块列表
	async getModule() {
		const result = await this.ctx.model.Access.find({
			"module_id": "0"
		});
		this.success(200, "请求成功", result)
	}

	async add() {
		// console.log(this.ctx.request.body);
		try {
			let addResult = this.ctx.request.body;
			let module_id = addResult.module_id;
			//菜单  或者操作
			if (module_id != 0) {
				addResult.module_id = this.app.mongoose.Types.ObjectId(module_id); //调用mongoose里面的方法把字符串转换成ObjectId
			}
			let access = new this.ctx.model.Access(addResult);
			access.save();
			await this.success(200, '新增权限成功');
		} catch (error) {
			console.log(error)
		}
	}

	async edit() {
		/*
		{
		  id: '5b8e4422b3cc641f4894d7bc',
		  module_name: '权限管理111',
		  type: '3',
		  action_name: '增加权限1',
		  url: '/admin/access/add',
		  module_id: '5b8e3836f71aad20249c2f98',
		  sort: '100',
		  description: '增加权限---操作1111' }
		*/
		let updateResult = this.ctx.query;
		let _id = updateResult._id;
		let module_id = updateResult.module_id;
		//菜单  或者操作
		if (module_id != 0) {
			updateResult.module_id = this.app.mongoose.Types.ObjectId(module_id); //调用mongoose里面的方法把字符串转换成ObjectId
		}
		let result = await this.ctx.model.Access.updateOne({
			"_id": _id
		}, updateResult);
		await this.success(200, '修改权限成功');
	}
	async delete() {
		let _id = this.ctx.query._id;
		const result = await this.ctx.model.Access.deleteOne({
			"_id": _id
		});
		if (result.deletedCount != 0) {
			this.success(200, '删除权限成功')
		} else {
			this.error(303, '删除权限失败')
		}
	}
}

module.exports = AccessController;
