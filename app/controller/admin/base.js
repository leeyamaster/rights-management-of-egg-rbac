'use strict';
const Controller = require('egg').Controller;
class BaseController extends Controller {
    async success(code = 200, msg = '请求成功', data, total) {
        const d = {
            code,
            msg,
            data
        };
        if (total) {
            d.total = total;
        }
        this.ctx.body = d;
    }

    async error(code = 303, msg = '请求失败', data = {}) {
        const d = {
            code,
            msg,
            data
        };
        this.ctx.body = d;
    }
}

module.exports = BaseController;
