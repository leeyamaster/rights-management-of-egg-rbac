var url = require('url');

module.exports = (options, app) => {
    return async function adminauth(ctx, next) {
        const pathname = url.parse(ctx.request.url).pathname;
        if (ctx.session.userinfo) {
            // ctx.state.userinfo = ctx.session.userinfo;  //全局变量
            const hasAuth = await ctx.service.admin.checkAuth();
            if (hasAuth) {
                await next();
            } else {
                const d = {
                    code: 500,
                    msg: '您没有权限访问当前地址'
                };
                ctx.body = d;
            }
        } else {
            //排除不需要做权限判断的页面  /admin/verify?mt=0.7466881301614958
            if (pathname == '/admin/login' || pathname == '/admin/verify'
			|| pathname == '/admin/loginOur'|| pathname == '/admin/getRoleId') {
                await next();
            } else {
                const d = {
                    code: 500,
                    msg: '请您登录'
                };
                ctx.body = d;
            }
        }
    };
};
