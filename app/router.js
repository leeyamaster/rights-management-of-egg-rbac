'use strict';
module.exports = app => {
    const {router, controller} = app;
    // var adminauth=app.middleware.adminauth()
    router.get('/', controller.home.index);
    router.post('/admin/login', controller.admin.login.login);
    router.get('/admin/loginOut', controller.admin.login.loginOut);
    router.get('/admin/verify', controller.admin.login.verify);
    router.get('/admin/getRoleId', controller.admin.manager.getRoleId);
    router.get('/admin/getAllRole', controller.admin.manager.getAllRole);
    router.get('/admin/manager', controller.admin.manager.index);
    router.post('/admin/manager', controller.admin.manager.add);
    router.put('/admin/manager', controller.admin.manager.edit);
    router.delete('/admin/manager', controller.admin.manager.delete);
    router.get('/admin/role', controller.admin.role.index);
    router.post('/admin/role', controller.admin.role.add);
    router.put('/admin/role', controller.admin.role.edit);
    router.delete('/admin/role', controller.admin.role.delete);
    router.post('/admin/role/auth', controller.admin.role.auth);
    router.get('/admin/access', controller.admin.access.index);
    router.get('/admin/access/module', controller.admin.access.getModule);
    router.post('/admin/access', controller.admin.access.add);
    router.put('/admin/access/', controller.admin.access.edit);
    router.delete('/admin/access/', controller.admin.access.delete);
};
