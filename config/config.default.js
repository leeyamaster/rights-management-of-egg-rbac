'use strict';

module.exports = appInfo => {
	const config = (exports) = {};

	// use for cookie sign key, should change to your own and keep security
	config.keys = appInfo.name + '_1534304805936_5738';

	config.session = {
		key: 'SESSION_ID',
		maxAge: 864000,
		httpOnly: true,
		encrypt: true,
		renew: true //  延长会话有效期       
	}

	config.security = {
		csrf: {
			enable: false,
			ignoreJSON: true
		},
		// 允许访问接口的白名单
		domainWhiteList: ['*'] // ['http://localhost:8080']
	}
	// add your config here
	config.middleware = ['adminauth'];

	config.adminauth = {
		match: '/admin',
	}
	config.cors = {
		origin: 'http://127.0.0.1:4200', //一定要是域名端口
		credentials: true, //credentials设置为true,和前端保持一致
		allowMethods: 'GET,POST,PUT,DELETE'
	}
	//配置mongose连接mongodb数据库
	exports.mongoose = {
		client: {
			url: 'mongodb://127.0.0.1/rbac-egg',
			options: {},
		}
	};

	return config;
};
